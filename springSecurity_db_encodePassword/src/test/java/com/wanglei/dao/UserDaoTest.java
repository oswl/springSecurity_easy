package com.wanglei.dao;

import com.wanglei.domain.UserInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml","classpath:spring-jdbc.xml","classpath:spring-security.xml"})
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void testQueryByUsername(){
        UserInfo userInfo = userDao.findByUsername("user");
        System.out.println(userInfo);
    }

    @Test
    public void testPasswordEncoder(){
        System.out.println(passwordEncoder.encode("123456"));
    }
}
