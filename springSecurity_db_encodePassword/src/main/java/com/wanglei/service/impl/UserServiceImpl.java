package com.wanglei.service.impl;

import com.wanglei.dao.UserDao;
import com.wanglei.domain.Role;
import com.wanglei.domain.UserInfo;
import com.wanglei.service.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户服务实现
 *
 * @author wl
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查询用户信息，需要包含角色信息，资源权限信息
        UserInfo userInfo = userDao.findByUsername(username);
        // 获取角色信息集合
        List<Role> roles = userInfo.getRoles();
        // 获取资源权限信息
        List<SimpleGrantedAuthority> authoritys = getAuthority(roles);
        /*
            password:当前密码使用的是加密后的密文，直接使用
            enabled:用户是否开启
            accountNonExpired:账户不过期
            credentialsNonExpired：凭证不过期
            accountNonLocked：非锁定账户
            authorities：拥有角色名称集合
         */
        return new User(
                userInfo.getUsername(),
                userInfo.getPassword(),
                userInfo.getStatus() != 0,
                true,
                true,
                true,
                authoritys);
    }

    private List<SimpleGrantedAuthority> getAuthority(List<Role> roles) {
        List<SimpleGrantedAuthority> authoritys = new ArrayList<>();
        for (Role role : roles) {
            // 给角色名称拼上ROLE_前缀
            authoritys.add(new SimpleGrantedAuthority("ROLE_"+role.getRoleName()));
        }
        return authoritys;
    }
}
