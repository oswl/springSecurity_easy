package com.wanglei.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 用户服务继承用户认证服务
 *
 * @author wl
 */
public interface UserService extends UserDetailsService {
}
