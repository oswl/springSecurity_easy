package com.wanglei.dao;

import com.wanglei.domain.UserInfo;

/**
 * 用户数据操作对象
 *
 * @author wl
 */
public interface UserDao {
    /**
     * 根据用户名查询用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    UserInfo findByUsername(String username);
}
