-- 下面三步需要在dba用户下执行，datafile路径需要在服务器已存在,如果有如下需求，可放开注释在dba权限的用户下执行，例如sys
-- -- 创建表空间
-- create tablespace spring_security_easy
--     datafile 'c:\oracle\data\spring_security_easy.dbf'
--     size 100 m
--     autoextend on
--     next 10 m;
-- -- 创建用户
-- create user test
--     identified by "123456"
--     default tablespace spring_security_easy;
-- -- 赋权
-- grant connect, resource to test;

-- 用户相关表
-- 先清空已有表
drop table users_role;
drop table users;
drop table role_permission;
drop table role;
drop table permission;

-- 创建用户表并插入注释
create table users(
    id varchar2(32) default sys_guid() primary key,
    email varchar2(50) unique not null,
    username varchar2(50),
    password varchar2(50),
    phone_num varchar2(20),
    status int
);
comment on table users is '用户表';
comment on column users.id is '用户id,主键';
comment on column users.email is '邮件地址，非空，唯一';
comment on column users.username is '用户名';
comment on column users.PASSWORD is '密码（加密）';
comment on column users.phone_num is '电话号码';
comment on column users.STATUS is '状态0未开启1开启';

-- 创建角色并插入注释
create table role(
    id varchar2(32) default sys_guid() primary key,
    role_name varchar2(50) ,
    role_desc varchar2(50)
);
comment on table role is '角色表';
comment on column role.id is '角色id';
comment on column role.role_name is '角色名';
comment on column role.role_desc is '角色描述';

-- 创建用户与角色关联表并插入注释
CREATE TABLE users_role(
    user_id varchar(32),
    role_id varchar(32),
    primary key(user_id,role_id),
    foreign key (user_id) references users(id),
    foreign key (role_id) references role(id)
);
comment on table users_role is '用户与角色关系表';
comment on column users_role.user_id is '用户id';
comment on column users_role.role_id is '角色id';

-- 创建资源权限表并插入注释
create table permission(
    id varchar2(32) default sys_guid() primary key,
    permission_name varchar2(50) ,
    url varchar2(50)
);
comment on table permission is '资源权限表';
comment on column permission.id is '主键uuid';
comment on column permission.permission_name is '权限名';
comment on column permission.url is '资源路径';

-- 创建资源权限与角色关联表并插入注释
create table role_permission(
    permission_id varchar2(32),
    role_id varchar2(32),
    primary key(permission_id,role_id),
    foreign key (permission_id) references permission(id),
    foreign key (role_id) references role(id)
);
comment on table role_permission is '资源权限与角色关联表';
comment on column role_permission.permission_id is '资源权限表id';
comment on column role_permission.role_id is '角色表id';

-- 提交事务
commit;