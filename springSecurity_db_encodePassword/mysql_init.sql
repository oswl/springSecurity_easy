-- 假如不存在用户，创建一个用户并赋权，记得在root权限下操作
create user if not exists 'test' identified by '123456';
grant all on *.* to 'test';

-- 数据库初始化
drop database if exists spring_security_easy;
create database if not exists spring_security_easy character set 'utf8' collate 'utf8_general_ci';
use spring_security_easy;

-- 设置数据库当前编码格式
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- 创建用户表
drop table if exists users_role;
drop table if exists users;
create table users(
    id varchar(32) primary key comment '唯一标识，uuid',
    email varchar(50) unique not null comment '邮件',
    username varchar(50) comment '用户名',
    password varchar(128) comment '密码（加密）',
    phone_num varchar(20) comment '电话号码',
    status int comment '用户状态（0开启1关闭）'
) comment '用户表';

-- 创建角色表
drop table if exists role_permission;
drop table if exists role;
create table role(
    id varchar(32) primary key comment '唯一标识，uuid',
    role_name varchar(50) comment '角色名称',
    role_desc varchar(50) comment '角色描述'
) comment '角色表';

-- 创建用户角色关联表，多对多关系
create table users_role(
    user_id varchar(32) comment '外键：用户id',
    role_id varchar(32) comment '外键：角色id',
    primary key(user_id,role_id),
    foreign key (user_id) references users(id),
    foreign key (role_id) references role(id)
) comment '用户角色关联表';

-- 创建资源权限表
drop table if exists permission;
create table permission(
   id varchar(32) primary key comment '唯一标识，uuid',
   permission_name varchar(50) comment '权限名称',
   url varchar(50) comment '资源路径'
) comment '资源权限表';

-- 创建角色与权限资源关联表
create table role_permission(
    role_id varchar(32) comment '外键：角色id',
    permission_id varchar(32) comment '外键：资源权限id',
    primary key(permission_id,role_id),
    foreign key (permission_id) references permission(id),
    foreign key (role_id) references role(id)
) comment '角色与权限资源关联表';

-- 插入用户数据
delete from users_role where 1=1;
delete from users where 1=1;
INSERT INTO `spring_security_easy`.`users`(`id`, `email`, `username`, `password`, `phone_num`, `status`) VALUES ('AE475437D90911EB8BC23C7C3F2B8063', 'bxc@lxz.com', '杜凌菲', '$2a$10$xymInPg5YzIgrvR.lPbcROz.nE6MzH92An5iUBViVBUGYDct.FFxS', '15166669999', 1);
INSERT INTO `spring_security_easy`.`users`(`id`, `email`, `username`, `password`, `phone_num`, `status`) VALUES ('AE48A0A1D90911EB8BC23C7C3F2B8063', 'user@lxz.com', 'user', '$2a$10$xymInPg5YzIgrvR.lPbcROz.nE6MzH92An5iUBViVBUGYDct.FFxS', '15114789632', 1);
INSERT INTO `spring_security_easy`.`users`(`id`, `email`, `username`, `password`, `phone_num`, `status`) VALUES ('AE4A9C21D90911EB8BC23C7C3F2B8063', 'admin@lxz.com', 'admin', '$2a$10$xymInPg5YzIgrvR.lPbcROz.nE6MzH92An5iUBViVBUGYDct.FFxS', '15156487895', 1);

-- 插入角色数据
delete from role where 1=1;
INSERT INTO `spring_security_easy`.`role`(`id`, `role_name`, `role_desc`) VALUES ('AE4CDB58D90911EB8BC23C7C3F2B8063', 'TEST', '测试角色');
INSERT INTO `spring_security_easy`.`role`(`id`, `role_name`, `role_desc`) VALUES ('AE4DEB4DD90911EB8BC23C7C3F2B8063', 'USER', '普通角色');
INSERT INTO `spring_security_easy`.`role`(`id`, `role_name`, `role_desc`) VALUES ('AE4EDEC3D90911EB8BC23C7C3F2B8063', 'ADMIN', '管理员角色');

-- 插入用户和角色关联信息
INSERT INTO `spring_security_easy`.`users_role`(`user_id`, `role_id`) VALUES ('AE475437D90911EB8BC23C7C3F2B8063', 'AE4CDB58D90911EB8BC23C7C3F2B8063');
INSERT INTO `spring_security_easy`.`users_role`(`user_id`, `role_id`) VALUES ('AE48A0A1D90911EB8BC23C7C3F2B8063', 'AE4DEB4DD90911EB8BC23C7C3F2B8063');
INSERT INTO `spring_security_easy`.`users_role`(`user_id`, `role_id`) VALUES ('AE4A9C21D90911EB8BC23C7C3F2B8063', 'AE4EDEC3D90911EB8BC23C7C3F2B8063');
