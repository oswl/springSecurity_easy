# SpringSecurity快速入门

## 1、前言：

启动后有一个SpringSecurity提供的登录界面，可以输入用户名和密码登录

登录成功，跳转到hello world界面

登录失败，提示错误

## 2、Spring Security介绍

- Spring 是一个非常流行和成功的java应用开发框架。 

- Spring Security 基于Spring 框架，提供了一套web应用安全性的完整解决方案。

- 一般来说，Web 应用的安全性包括两部分：

  - 用户**认证**（Authentication）

    ​		用户认证指的是验证某个用户是否为系统中的合法主体，也就是说用户能否访问该系统。用户认证一般要求用户提供用户名和密码。系统通过校验用户名和密码来完成认证过程。

  - 用户**授权**（Authorization）

    ​		用户授权指的是验证某个用户是否有权限执行某个操作。在一个系统中，不同用户所具有的权限是不同的。比如对一个文件来说，有的用户只能进行读取，而有的用户可以进行修改。

    ​			一般来说，系统会为不同的用户分配不同的角色，而每个角色则对应一系列的权限。

- 对于上面提到的两种应用情景，Spring Security 框架都有很好的支持。

- 在用户**认证**方面，Spring Security 框架支持主流的认证方式，包括 HTTP 基本认证、HTTP 表单验证、HTTP 摘要认证、OpenID 和 LDAP 等。

- 在用户**授权**方面，Spring Security 提供了基于角色的访问控制和访问控制列表（Access Control List，ACL），可以对应用中的领域对象进行细粒度的控制。

## 3、快速入门三步走

### a) 引入依赖

```xml
<properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <spring.version>5.0.4.RELEASE</spring.version>
    <spring.security.version>5.0.2.RELEASE</spring.security.version>
</properties>
<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-web</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context-support</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-web</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-config</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>3.1.0</version>
        <scope>provided</scope>
    </dependency>
</dependencies>
<build>
    <finalName>springSecurity_quickStart</finalName>
    <pluginManagement>
        <plugins>       
            <!-- java编译插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.2</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat7-maven-plugin</artifactId>
                <configuration>
                    <!-- 指定端口 -->
                    <port>9999</port>
                    <!-- 请求路径 -->
                    <path>/</path>
                </configuration>
            </plugin>
        </plugins>
    </pluginManagement>
</build>
```

### b) 配置springSecurity配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:security="http://www.springframework.org/schema/security"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd
          http://www.springframework.org/schema/security
          http://www.springframework.org/schema/security/spring-security.xsd">
    <!--
        该标签的含义简单来说：是对资源路径作为权限赋予给角色
        本配置是将所有资源路径都赋值给ROLE_USER,因此使用带有该角色权限的用户登录都可以通过认证
        auto-config:开启自动配置，会提供登录页面和登录成功页面
        use-expressions="true"(默认)，应该按下列格式书写权限配置
            <security:intercept-url pattern="/**" access="hasRole('ROLE_USER')"/>
        use-expressions="false",书写格式如下
            <security:intercept-url pattern="/**" access="ROLE_USER"/>
    -->
    <security:http auto-config="true" use-expressions="false">
        <!-- intercept-url定义一个过滤规则
             pattern表示对哪些url进行权限控制，
             ccess属性表示在请求对应的URL时需要什么权限，默认配置时它应该是一个以逗号分隔的角色列表，请求的用户只需拥有其中的一个角色就能成功访问对应的URL  -->
        <security:intercept-url pattern="/**" access="ROLE_USER"/>
    </security:http>
    <!--配置认证管理器-->
    <security:authentication-manager>
        <!--配置认证管理器用户信息-->
        <security:authentication-provider>
            <!--配置用户列表，表示user用户密码是user,角色是ROLE_USER;admin用户密码是admin,角色是ROLE_ADMIN-->
            <security:user-service>
                <!--name:用户名 password:密码 authorities:角色名称-->
                <security:user name="user" password="{noop}user" authorities="ROLE_USER"/>
                <security:user name="admin" password="{noop}admin" authorities="ROLE_ADMIN"/>
            </security:user-service>
        </security:authentication-provider>
    </security:authentication-manager>
</beans>
```

### c) 配置web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">
    <display-name>springSecurity_quickStart</display-name>
    <!--初始化spring配置文件-->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:spring-security.xml</param-value>
    </context-param>
    <!--配置spring核心监听-->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
    <!--配置springSecurity核心过滤器，拦截所有请求,注意springSecurityFilterChain是固定的，不能改变-->
    <filter>
        <filter-name>springSecurityFilterChain</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>springSecurityFilterChain</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>
```

### d) 启动程序

命令行使用`mvn tomcat7:run`启动程序

## 4、界面演示

 ![image-20210628153547387](readme/img/image-20210628153547387.png)

### a) 使用user/user用户登录，角色是ROLE_USER,密码正确输入

 ![image-20210628153705584](readme/img/image-20210628153705584.png)

认证通过，登录成功

![image-20210628153721296](readme/img/image-20210628153721296.png) 

### b)使用user/user用户登录，角色是ROLE_USER,密码错误输入

 ![image-20210628153843655](readme/img/image-20210628153843655.png)

认证失败，提示错误

 ![image-20210628153915047](readme/img/image-20210628153915047.png)

### c)使用admin/admin登录，角色是ROLE_ADMIN，密码正确输入登录

 ![image-20210628154106911](readme/img/image-20210628154106911.png)

报403无权限错误

 ![image-20210628154157543](readme/img/image-20210628154157543.png)

d)使用admin/admin登录，角色是ROLE_ADMIN，密码错误输入登录

 ![image-20210628154249711](readme/img/image-20210628154249711.png)

认证错误报错

 ![image-20210628154316191](readme/img/image-20210628154316191.png)