# SpringSecurity入门项目简介

## <a href="springSecurity_quickStart/readme.md">1、SpringSecurity快速入门</a>

## <a href="springSecurity_customPage/readme.md">2、SpringSecurity-自定义页面</a>

## <a href="springSecurity_db/readme.md">3、SpringSecurity-使用数据库存储权限信息(明文)</a>

## <a href="springSecurity_db_encodePassword/readme.md">4、SpringSecurity-使用数据库存储权限信息(密文)</a>

