package com.wanglei.dao;

import com.wanglei.domain.UserInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml","classpath:spring-jdbc.xml"})
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void testQueryByUsername(){
        UserInfo userInfo = userDao.findByUsername("白小纯");
        System.out.println(userInfo);
    }
}
