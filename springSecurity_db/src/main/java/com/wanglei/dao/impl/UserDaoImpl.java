package com.wanglei.dao.impl;

import com.wanglei.dao.UserDao;
import com.wanglei.domain.Permission;
import com.wanglei.domain.Role;
import com.wanglei.domain.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户数据操作对象实现，使用jdbcTemplate完成数据持久化
 *
 * @author wl
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public UserInfo findByUsername(String username) {
        // 根据用户名查询用户信息
        String queryUserInfoSql = "select * from users where username = ?";
        UserInfo userInfo = jdbcTemplate.queryForObject(queryUserInfoSql, new Object[]{username}, new BeanPropertyRowMapper<>(UserInfo.class));

        // 根据用户名查询角色信息
        String queryRolesByUsernameSql = "select * from role where id in (select role_id from users_role where user_id in (select id from users where username = ?)) ";
        List<Role> roles = jdbcTemplate.query(queryRolesByUsernameSql, new Object[]{username}, new BeanPropertyRowMapper<>(Role.class));
        if (userInfo != null) {
            userInfo.setRoles(roles);
        }

        // 如果有角色信息，根据用户名查询资源权限信息
        if (roles != null && roles.size() >= 1) {
            String queryPermissionByUsernameSql = "select * from permission where id in (select permission_id from role_permission where role_id = ?)";
            for (Role role : roles) {
                List<Permission> permissions = jdbcTemplate.query(queryPermissionByUsernameSql,new Object[]{role.getId()},new BeanPropertyRowMapper<>(Permission.class));
                role.setPermissions(permissions);
            }
        }
        return userInfo;
    }
}
