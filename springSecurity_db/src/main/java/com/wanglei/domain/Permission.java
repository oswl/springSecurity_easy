package com.wanglei.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 资源权限对象
 *
 * @author wl
 */
public class Permission implements Serializable {
    /**
     * 唯一id
     */
    private String id;
    /**
     * 资源权限名称
     */
    private String permissionName;
    /**
     * 资源路径
     */
    private String url;
    /**
     * 角色信息集合
     */
    private List<Role> roles;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "id='" + id + '\'' +
                ", permissionName='" + permissionName + '\'' +
                ", url='" + url + '\'' +
                ", roles=" + roles +
                '}';
    }
}
