# SpringSecurity-使用数据库存储权限信息(明文)

## 1、前言：

目标：SpringSecurity接入数据库，使用数据库用户权限数据，实现登录认证操作。

## 2、初始化五张权限表

mysql

### 数据库及用户

```sql
-- 假如不存在用户，创建一个用户并赋权，记得在root权限下操作
create user if not exists 'test' identified by '123456';
grant all on *.* to 'test';

-- 数据库初始化
drop database if exists spring_security_easy;
create database if not exists spring_security_easy character set 'utf8' collate 'utf8_general_ci';
```

### 权限表

```sql
-- 创建用户表
drop table if exists users_role;
drop table if exists users;
create table users(
    id varchar(32) primary key comment '唯一标识，uuid',
    email varchar(50) unique not null comment '邮件',
    username varchar(50) comment '用户名',
    password varchar(50) comment '密码（加密）',
    phone_num varchar(20) comment '电话号码',
    status int comment '用户状态（0开启1关闭）'
) comment '用户表';

-- 创建角色表
drop table if exists role_permission;
drop table if exists role;
create table role(
    id varchar(32) primary key comment '唯一标识，uuid',
    role_name varchar(50) comment '角色名称',
    role_desc varchar(50) comment '角色描述'
) comment '角色表';

-- 创建用户角色关联表，多对多关系
create table users_role(
    user_id varchar(32) comment '外键：用户id',
    role_id varchar(32) comment '外键：角色id',
    primary key(user_id,role_id),
    foreign key (user_id) references users(id),
    foreign key (role_id) references role(id)
) comment '用户角色关联表';

-- 创建资源权限表
drop table if exists permission;
create table permission(
   id varchar(32) primary key comment '唯一标识，uuid',
   permission_name varchar(50) comment '权限名称',
   url varchar(50) comment '资源路径'
) comment '资源权限表';

-- 创建角色与权限资源关联表
create table role_permission(
    role_id varchar(32) comment '外键：角色id',
    permission_id varchar(32) comment '外键：资源权限id',
    primary key(permission_id,role_id),
    foreign key (permission_id) references permission(id),
    foreign key (role_id) references role(id)
) comment '角色与权限资源关联表';
```

### 初始化用户权限数据

```sql
-- 插入用户数据
INSERT INTO `spring_security_easy`.`users`(`id`, `email`, `username`, `password`, `phone_num`, `status`) VALUES ('1', 'bxc@lxz.com', '白小纯', '123456', '15166669999', 1);
INSERT INTO `spring_security_easy`.`users`(`id`, `email`, `username`, `password`, `phone_num`, `status`) VALUES ('2', 'user@lxz.com', 'user', '123456', '15114789632', 1);
INSERT INTO `spring_security_easy`.`users`(`id`, `email`, `username`, `password`, `phone_num`, `status`) VALUES ('3', 'admin@lxz.com', 'admin', '123456', '15156487895', 1);

-- 插入角色数据
INSERT INTO `spring_security_easy`.`role`(`id`, `role_name`, `role_desc`) VALUES ('1', 'TEST', '测试角色');
INSERT INTO `spring_security_easy`.`role`(`id`, `role_name`, `role_desc`) VALUES ('2', 'USER', '普通角色');
INSERT INTO `spring_security_easy`.`role`(`id`, `role_name`, `role_desc`) VALUES ('3', 'ADMIN', '管理员角色');

-- 插入用户和角色关联信息
INSERT INTO `spring_security_easy`.`users_role`(`user_id`, `role_id`) VALUES ('1', '1');
INSERT INTO `spring_security_easy`.`users_role`(`user_id`, `role_id`) VALUES ('2', '2');
INSERT INTO `spring_security_easy`.`users_role`(`user_id`, `role_id`) VALUES ('3', '3');
```

## 3、代码编写

### a、引入依赖

#### pom.xml

```xml
<properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <spring.version>5.0.4.RELEASE</spring.version>
    <spring.security.version>5.0.2.RELEASE</spring.security.version>
    <log4j.version>1.2.17</log4j.version>
    <slf4j.version>2.0.0-alpha1</slf4j.version>
</properties>
<dependencies>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13.2</version>
        <scope>test</scope>
    </dependency>
    <!--spring相关-->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-web</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context-support</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <!--spring-security相关-->
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-web</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-config</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <!--web编译支持-->
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>3.1.0</version>
        <scope>provided</scope>
    </dependency>
    <!--mysql数据库支持-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.24</version>
    </dependency>
    <!--log4j支持-->
    <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>${log4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-log4j12</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
</dependencies>
<build>
    <finalName>springSecurity_db</finalName>
    <pluginManagement>
        <plugins>            
            <!-- java编译插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.2</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat7-maven-plugin</artifactId>
                <configuration>
                    <!-- 指定端口 -->
                    <port>9999</port>
                    <!-- 请求路径 -->
                    <path>/</path>
                </configuration>
            </plugin>
        </plugins>
    </pluginManagement>
</build>
```

### b、创建实体类

#### UserInfo.java

```java
/**
 * 用户对象
 *
 * @author wl
 */
public class UserInfo implements Serializable {
    /**
     * 用户id
     */
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 邮件地址
     */
    private String email;
    /**
     * 密码
     */
    private String password;
    /**
     * 电话号码
     */
    private String phoneNum;
    /**
     * 账户状态0未开启，1开启
     */
    private Integer status;
    /**
     * 账户状态中文描述
     */
    private String statusStr;
    /**
     * 角色信息集合
     */
    private List<Role> roles;
}
```

#### Permission.java

```java
/**
 * 资源权限对象
 *
 * @author wl
 */
public class Permission implements Serializable {
    /**
     * 唯一id
     */
    private String id;
    /**
     * 资源权限名称
     */
    private String permissionName;
    /**
     * 资源路径
     */
    private String url;
    /**
     * 角色信息集合
     */
    private List<Role> roles;
}
```

#### Role.java

```java
/**
 * 角色对象
 *
 * @author wl
 */
public class Role implements Serializable {
    /**
     * 唯一索引
     */
    private String id;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色描述
     */
    private String roleDesc;
    /**
     * 权限信息集合
     */
    private List<Permission> permissions;
    /**
     * 用户信息集合
     */
    private List<UserInfo> users;
}
```

### c、编写配置文件

##### applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd
           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
    <!--配置注解扫描dao和service-->
    <context:component-scan base-package="com.wanglei.dao"/>
    <context:component-scan base-package="com.wanglei.service"/>
</beans>
```

##### log4j.properties

```properties
# Set root category priority to INFO and its only appender to CONSOLE.
#log4j.rootCategory=INFO, CONSOLE            debug   info   warn error fatal
log4j.rootCategory=debug, CONSOLE

# Set the enterprise logger category to FATAL and its only appender to CONSOLE.
log4j.logger.org.apache.axis.enterprise=FATAL, CONSOLE

# CONSOLE is set to be a ConsoleAppender using a PatternLayout.
log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
log4j.appender.CONSOLE.layout.ConversionPattern=%d{ISO8601} %-6r [%15.15t] %-5p %30.30c %x - %m\n

# LOGFILE is set to be a File appender using a PatternLayout.
#log4j.appender.LOGFILE=org.apache.log4j.FileAppender
#log4j.appender.LOGFILE.File=axis.log
#log4j.appender.LOGFILE.Append=true
#log4j.appender.LOGFILE.layout=org.apache.log4j.PatternLayout
#log4j.appender.LOGFILE.layout.ConversionPattern=%d{ISO8601} %-6r [%15.15t] %-5p %30.30c %x - %m\n
```

##### spring-jdbc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd">
    <!--配置数据源-->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/spring_security_easy"/>
        <property name="username" value="test"/>
        <property name="password" value="123456"/>
    </bean>

    <!--给jdbcTemplate注入数据源-->
    <bean id="jdbc" class="org.springframework.jdbc.core.JdbcTemplate">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <!--配置事务管理器-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <!--开启注解式事务支持,proxy-target-class="true"设置使用cglib的代理方式-->
    <tx:annotation-driven proxy-target-class="true"/>
</beans>
```

##### spring-security.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:security="http://www.springframework.org/schema/security"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd
          http://www.springframework.org/schema/security
          http://www.springframework.org/schema/security/spring-security.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <!--配置注解扫描-->
    <context:component-scan base-package="com.wanglei"/>

    <!-- 配置不过滤的资源（静态资源及登录相关） -->
    <security:http security="none" pattern="/login.jsp" />
    <security:http security="none" pattern="/fail.jsp" />

    <security:http auto-config="true" use-expressions="false">
        <security:intercept-url pattern="/**" access="ROLE_USER"/>
        <!-- 自定义登陆页面，
             login-page 自定义登陆页面
             authentication-failure-url 用户权限校验失败之后才会跳转到这个页面，如果数据库中没有这个用户则不会跳转到这个页面。
             default-target-url 登陆成功后跳转的页面。
             注：登陆页面用户名固定 username，密码password，action:login
         -->
        <security:form-login login-page="/login.jsp"
                             login-processing-url="/login"
                             username-parameter="username"
                             password-parameter="password"
                             authentication-failure-url="/fail.jsp"
                             default-target-url="/success.jsp"
        />
        <!-- 登出，
             invalidate-session 是否删除
             session logout-url：登出处理链接
             logout-success-url：登出成功页面
             注：登出操作 只需要链接到 logout即可登出当前用户
         -->
        <security:logout invalidate-session="true" logout-url="/logout" logout-success-url="/login.jsp" />
        <!-- 关闭CSRF(跨域访问),默认是开启的 -->
        <security:csrf disabled="true" />
    </security:http>

    <!--配置认证管理器-->
    <security:authentication-manager>
        <!--配置认证管理器用户信息-->
        <security:authentication-provider user-service-ref="userService" />
    </security:authentication-manager>
</beans>
```

### d、编写自定义登录界面

login.jsp

```jsp
<%@page contentType="text/html; UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>login</title>
</head>
<body>
<form action="login" method="post">
    <table>
        <tr>
            <td>用户名：</td>
            <td><input type="text" name="username"/></td>
        </tr>
        <tr>
            <td>密码：</td>
            <td><input type="password" name="password"/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" value="登录"/>
                <input type="reset" value="重置"/>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
```

success.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>success</title>
</head>
<body>
    <h1>success html</h1>
    <a href="logout">退出</a>
</body>
</html>
```

fail.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1>登录失败</h1>
</body>
</html>
```

### e、编写web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">
    <display-name>springSecurity_db</display-name>
    <!--初始化spring配置文件-->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:applicationContext.xml,classpath:spring-jdbc.xml,classpath:spring-security.xml</param-value>
    </context-param>
    <!--配置spring核心监听-->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
    <!--配置springWeb的编码过滤器并配置编码格式为UTF-8-->
    <filter>
        <filter-name>characterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceRequestEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>characterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <!--配置springSecurity核心过滤器，拦截所有请求,注意springSecurityFilterChain是固定的，不能改变-->
    <filter>
        <filter-name>springSecurityFilterChain</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>springSecurityFilterChain</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <!--默认欢迎页是登录页面-->
    <welcome-file-list>
        <welcome-file>login.jsp</welcome-file>
    </welcome-file-list>
</web-app>
```

### f、编写dao层代码

UserDao.java

```java
/**
 * 用户数据操作对象
 *
 * @author wl
 */
public interface UserDao {
    /**
     * 根据用户名查询用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    UserInfo findByUsername(String username);
}
```

UserDaoImpl.java

```java
/**
 * 用户数据操作对象实现，使用jdbcTemplate完成数据持久化
 *
 * @author wl
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public UserInfo findByUsername(String username) {
        // 根据用户名查询用户信息
        String queryUserInfoSql = "select * from users where username = ?";
        UserInfo userInfo = jdbcTemplate.queryForObject(queryUserInfoSql, new Object[]{username}, new BeanPropertyRowMapper<>(UserInfo.class));

        // 根据用户名查询角色信息
        String queryRolesByUsernameSql = "select * from role where id in (select role_id from users_role where user_id in (select id from users where username = ?)) ";
        List<Role> roles = jdbcTemplate.query(queryRolesByUsernameSql, new Object[]{username}, new BeanPropertyRowMapper<>(Role.class));
        if (userInfo != null) {
            userInfo.setRoles(roles);
        }

        // 如果有角色信息，根据用户名查询资源权限信息
        if (roles != null && roles.size() >= 1) {
            String queryPermissionByUsernameSql = "select * from permission where id in (select permission_id from role_permission where role_id = ?)";
            for (Role role : roles) {
                List<Permission> permissions = jdbcTemplate.query(queryPermissionByUsernameSql,new Object[]{role.getId()},new BeanPropertyRowMapper<>(Permission.class));
                role.setPermissions(permissions);
            }
        }
        return userInfo;
    }
}
```

### g、编写service层代码

UserService extends UserDetailsService

```java
/**
 * 用户服务继承用户认证服务
 *
 * @author wl
 */
public interface UserService extends UserDetailsService {
}
```

UserServiceImpl

```java
/**
 * 用户服务实现
 *
 * @author wl
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查询用户信息，需要包含角色信息，资源权限信息
        UserInfo userInfo = userDao.findByUsername(username);
        // 获取角色信息集合
        List<Role> roles = userInfo.getRoles();
        // 获取资源权限信息
        List<SimpleGrantedAuthority> authoritys = getAuthority(roles);
        /*
            password:{noop} 前缀是密码为明文的时候添加的内容，后期加密密码后需要去除
            enabled:用户是否开启
            accountNonExpired:账户不过期
            credentialsNonExpired：凭证不过期
            accountNonLocked：非锁定账户
            authorities：拥有角色名称集合
         */
        return new User(userInfo.getUsername(),
                "{noop}" + userInfo.getPassword(),
                userInfo.getStatus() != 0,
                true,
                true,
                true,
                authoritys);
    }

    private List<SimpleGrantedAuthority> getAuthority(List<Role> roles) {
        List<SimpleGrantedAuthority> authoritys = new ArrayList();
        for (Role role : roles) {
            // 给角色名称拼上ROLE_前缀
            authoritys.add(new SimpleGrantedAuthority("ROLE_"+role.getRoleName()));
        }
        return authoritys;
    }
}
```

### h、启动项目，演示

#### 1.使用user/123456登录系统

 ![image-20210629172807467](readme/img/image-20210629172807467.png)

登录成功

 ![image-20210629172846147](readme/img/image-20210629172846147.png)

#### 2.退出登录

 ![image-20210629172916332](readme/img/image-20210629172916332.png)

退出到登录页

 ![image-20210629173013195](readme/img/image-20210629173013195.png)

#### 3.使用admin/123456登录

 ![image-20210629173110234](readme/img/image-20210629173110234.png)

无权限403

 ![image-20210629173141123](readme/img/image-20210629173141123.png)

#### 4.使用不存在的账户登录aaa/aa

 ![image-20210629173225634](readme/img/image-20210629173225634.png)

登录失败

 ![image-20210629173248467](readme/img/image-20210629173248467.png)

