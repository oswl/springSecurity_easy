# SpringSecurity-自定义页面

## 1、修改springSecurity配置文件

```xml
    <security:http auto-config="true" use-expressions="false">
        <security:intercept-url pattern="/**" access="ROLE_USER"/>
        <!-- 自定义登陆页面，
             login-page 自定义登陆页面
             authentication-failure-url 用户权限校验失败之后才会跳转到这个页面，如果数据库中没有这个用户则不会跳转到这个页面。
             default-target-url 登陆成功后跳转的页面。
             注：登陆页面用户名固定 username，密码password，action:login
         -->
        <security:form-login login-page="/login.html"
                             login-processing-url="/login"
                             username-parameter="username"
                             password-parameter="password"
                             authentication-failure-url="/fail.html"
                             default-target-url="/success.html"
        />
        <!-- 登出，
             invalidate-session 是否删除
             session logout-url：登出处理链接
             logout-success-url：登出成功页面
             注：登出操作 只需要链接到 logout即可登出当前用户
         -->
        <security:logout invalidate-session="true" 
                         logout-url="/logout" 
                         logout-success-url="/login.jsp" />
        <!-- 关闭CSRF(跨域访问),默认是开启的 -->
        <security:csrf disabled="true" />
    </security:http>
```

## 2、创建login.html,success.html,fail.html

login.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>login</title>
</head>
<body>
<form action="login" method="post">
    <table>
        <tr>
            <td>用户名：</td>
            <td><input type="text" name="username"/></td>
        </tr>
        <tr>
            <td>密码：</td>
            <td><input type="password" name="password"/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" value="登录"/>
                <input type="reset" value="重置"/>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
```

success.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>success</title>
</head>
<body>
    <h1>success html</h1>
    <a href="logout">退出</a>
</body>
</html>
```

fail.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1>登录失败</h1>
</body>
</html>
```

## 3、效果演示

 ![image-20210628161825146](readme/img/image-20210628161825146.png)

演示可以通过快速入门的例子自行操作查看效果
